//! # Webbby - The one web framework to rule them all!
//!
//! Entry to the crate is provided by `build()`

use std::io::Write;

/// # Build your site!
/// Takes a file and the HTML of your page
/// 
/// Example:
/// ```rust
/// use webbby::build;
/// use std::fs::File;
///
/// fn main() -> std::io::Result<()> {
///     let content = "<h1>Hello, World!</h1>
///     <p>Hyd Today?</p>";
///     let out = File::create("foo.txt")?;
///     build(out, content);
///     Ok(())
/// }
/// ```
pub fn build(mut f: std::fs::File, c: &str) {
    f.write_all(c.as_bytes()).unwrap();
}
